
export default class GListStoreModule {
  constructor( uri ) {

    this.api = api;

    this.state = {
      filters: {},
      uri: uri,
      resultList: null,
      page: 1,
      limit: 10,
      nbResultTotal: null,
      updating: false,
      loadingMore: false,
    };

    this.getters = {
      getResultList: state => {
        return state.resultList;
      },
      getPage: state => {
        return state.page;
      },
      getLimit: state => {
        return state.limit;
      },
      getNbResultTotal: state => {
        return state.nbResultTotal;
      },
      getUpdating: state => {
        return state.updating;
      },
      getLoadingMore: state => {
        return state.loadingMore;
      }, 
      getArgs: state => {
        return {
          limit: state.limit,
          page: state.page,
          ...state.filters,
        };
      },
      hasMore: state => {
        return state.resultList && state.resultList.length < state.nbResultTotal;
      },
      getFilters: state => {
        return state.filters;
      },
      getFilter: state => (filter) => {
        if (filter in state.filters) {
          return state.filters[filter];
        } else {
          return null;
        }
      },
    };

    this.mutations = {
      addFilter(state, filter) {
        state.filters = {
          ...state.filters,
          ...filter,
        };
      },
      removeFilter(state, filter) {
        delete state.filters[filter];
      },
      resetFilters(state) {
        state.filters = {};
      },
      locallyUpdateElement(state, element) {
        const resultList = state.resultList;
        const indexOfValInList = resultList.findIndex((e) => {
          return element.id === e.id;
        });

        resultList[indexOfValInList] = element;
      },
    };

    this.actions = {
      update(context) {
        context.state.updating = true;
        context.state.page = 1;
        return this.apiapi.get(`${context.state.uri}?${encode (context.getters.getArgs)}`)
          .then((response) => {
            context.state.resultList = response.data.results;
            context.state.nbResultTotal = response.data.nb_results;
            context.state.updating = false;
          });
      },
      loadMore(context) {
        context.state.loadingMore = true;
        context.state.page = context.state.page + 1;
        return this.apiapi.get(`${context.state.uri}?${encode (context.getters.getArgs)}`)
          .then((response) => {
            context.state.resultList = context.state.resultList.concat(response.data.results);
            context.state.nbResultTotal = response.data.nb_results;
            context.state.loadingMore = false;
          });
      },
    };

  }

}

function encode(obj, prefix) {
  let str = [];
  for (let p in obj) {
    // eslint-disable-next-line
    if (obj.hasOwnProperty(p) && obj[p] !== null) {
      const k = prefix || p;
      const v = obj[p];
      str.push((v !== null && typeof v === "object") ? encode(v, k) : encodeURIComponent(k) + "=" + encodeURIComponent(v));
    }
  }
  return str.join("&");
}