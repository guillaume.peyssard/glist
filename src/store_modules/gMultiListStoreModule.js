
export default class GMultiListStoreModule {
  constructor( api, uri, initialCategories = [] ) {

    this.api = api;

    var lists = {};

    for(let category of initialCategories) {
      lists[category] = {
        resultList: null,
        page: 1,
        limit: 20,
        nbResultTotal: null,
        updating: false,
        loadingMore: false,
      };
    }

    this.state = {
      lists: lists,
      filters: {},
      uri: uri,
    };

    this.getters = {
      getResultList: state => (category) => {
        if (state.lists[category]) {
          return state.lists[category].resultList;
        } else {
          return null;
        }
      },
      getPage: state => (category) => {
        if (state.lists[category]) {
          return state.lists[category].page;
        } else {
          return 1;
        }
      },
      getLimit: state => (category) => {
        if (state.lists[category]) {
          return state.lists[category].limit;
        } else {
          return 10;
        }
      },
      getNbResultTotal: state => (category) => {
        if (state.lists[category]) {
          return state.lists[category].nbResultTotal;
        } else {
          return null;
        }
      },
      getUpdating: state => (category) => {
        if (state.lists[category]) {
          return state.lists[category].updating;
        } else {
          return false;
        }
      },
      getLoadingMore: state => (category) => {
        if (state.lists[category]) {
          return state.lists[category].loadingMore;
        } else {
          return false;
        }
      }, 
      getArgs: state => (category)=> {
        if (state.lists[category]) {
          return {
            limit: state.lists[category].limit,
            page: state.lists[category].page,
            ...state.filters,
          };
        } else {
          return {
            limit: 10,
            page: 1,
            ...state.filters,
          };
        }
      },
      hasMore: state => (category) => {
        if (state.lists[category] && state.lists[category].resultList != null && state.lists[category].nbResultTotal != null) {
          return state.lists[category].resultList.length < state.lists[category].nbResultTotal;
        } else {
          return false;
        }
      },
      getFilters: state => {
        return state.filters;
      },
      getFilter: state => (filter) => {
        if (filter in state.filters) {
          return state.filters[filter];
        } else {
          return null;
        }
      },
    };

    this.mutations = {
      addFilter(state, filter) {
        state.filters = {
          ...state.filters,
          ...filter,
        };
      },
      removeFilter(state, filter) {
        delete state.filters[filter];
      },
      resetFilters(state) {
        state.filters = {};
      },
      locallyUpdateElement(state, element) {
        for (let category in state.lists) {
          const resultList = state.lists[category].resultList;
          
          if (resultList != null) {
            const indexOfValInList = resultList.findIndex((e) => {
              return element.id === e.id;
            });

            resultList[indexOfValInList] = element;
          }
        }
      },
    };

    this.actions = {
      updateAll(context) {
        for (let category in context.state.lists) {
          context.state.lists[category].updating = true;
          context.state.lists[category].page = 1;
          this.api.get(`${context.state.uri}?${encode (context.getters.getArgs(category))}`)
            .then((response) => {
              context.state.lists[category].resultList = response.data.results;
              context.state.lists[category].nbResultTotal = response.data.nb_results;
              context.state.lists[category].updating = false;
            });
        }
      },
      update(context, category) {
        if (!context.state.lists[category]) {
          context.state.lists[category] = {
            resultList: null,
            page: 1,
            limit: 10,
            nbResultTotal: null,
            updating: false,
            loadingMore: false,
          };
        }

        context.state.lists[category].updating = true;
        context.state.lists[category].page = 1;
        return this.api.get(`${context.state.uri}?${encode (context.getters.getArgs(category))}`)
          .then((response) => {
            context.state.lists[category].resultList = response.data.results;
            context.state.lists[category].nbResultTotal = response.data.nb_results;
            context.state.lists[category].updating = false;
          });
      },
      loadMore(context, category) {
        if (!context.state.lists[category]) {
            throw new Error("Category not found");
        }
        context.state.lists[category].loadingMore = true;
        context.state.lists[category].page = context.state.lists[category].page + 1;
        return this.api.get(`${context.state.uri}?${encode (context.getters.getArgs(category))}`)
          .then((response) => {
            context.state.lists[category].resultList = context.state.lists[category].resultList.concat(response.data.results);
            context.state.lists[category].nbResultTotal = response.data.nb_results;
            context.state.lists[category].loadingMore = false;
          });
      },
    };

  }

}

function encode(obj, prefix) {
  let str = [];
  for (let p in obj) {
    // eslint-disable-next-line
    if (obj.hasOwnProperty(p) && obj[p] !== null) {
      const k = prefix || p;
      const v = obj[p];
      str.push((v !== null && typeof v === "object") ? encode(v, k) : encodeURIComponent(k) + "=" + encodeURIComponent(v));
    }
  }
  return str.join("&");
}